package com.agiletestingalliance;
import org.junit.Test;
import static org.junit.Assert.*;

public class UsefulnessTest{
        @Test
	public void MatchStringFound() {
	Usefulness about = new Usefulness();

                String Actual = about.desc();
                String Expected = "DevOps is about transformation, about building quality in, improving productivity and about automation in Dev, Testing and Operations. <br> <br> CP-DOF is a one of its kind initiative to marry 2 distinct worlds of Agile and Operations together. <br> <br> <b>CP-DOF </b> helps you learn DevOps fundamentals along with Continuous Integration and Continuous Delivery and deep dive into DevOps concepts and mindset.";
        assertEquals("CP-DOF content Matches",Expected,Actual);
        }
}
	

